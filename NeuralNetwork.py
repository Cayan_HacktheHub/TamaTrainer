import math
import random
import matplotlib.pyplot as plt

BIAS = -1


def mapToRange(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min


class Neuron:
    def __init__(self, n_inputs):
        self.n_inputs = n_inputs
        self.set_weights([random.uniform(0, 1)
                          for x in range(0, n_inputs + 1)])  # +1 for bias weight

    def sum(self, inputs):
        # Does not include the bias
        return sum(val * self.weights[i] for i, val in enumerate(inputs))

    def set_weights(self, weights):
        self.weights = weights

    def __str__(self):
        return 'Weights: %s, Bias: %s' % (str(self.weights[:-1]), str(self.weights[-1]))


class NeuronLayer:
    def __init__(self, n_neurons, n_inputs):
        self.n_neurons = n_neurons
        self.neurons = [Neuron(n_inputs) for _ in range(0, self.n_neurons)]

    def __str__(self):
        return 'Layer:\n\t' + '\n\t'.join([str(neuron) for neuron in self.neurons]) + ''


class NeuralNetwork:
    def __init__(self, n_inputs, n_outputs, n_neurons_to_hl, n_hidden_layers):
        self.n_inputs = n_inputs
        self.n_outputs = n_outputs
        self.n_hidden_layers = n_hidden_layers
        self.n_neurons_to_hl = n_neurons_to_hl

        # Do not touch
        self._create_network()
        self._n_weights = None
        # end

    def _create_network(self):
        if self.n_hidden_layers > 0:
            # create the first layer
            self.layers = [NeuronLayer(self.n_neurons_to_hl, self.n_inputs)]

            # create hidden layers
            self.layers += [NeuronLayer(self.n_neurons_to_hl, self.n_neurons_to_hl)
                            for _ in range(0, self.n_hidden_layers)]

            # hidden-to-output layer
            self.layers += [NeuronLayer(self.n_outputs, self.n_neurons_to_hl)]
        else:
            # If we don't require hidden layers
            self.layers = [NeuronLayer(self.n_outputs, self.n_inputs)]

    def get_weights(self):
        weights = []

        for layer in self.layers:
            for neuron in layer.neurons:
                weights += neuron.weights

        return weights

    @property
    def n_weights(self):
        if not self._n_weights:
            self._n_weights = 0
            for layer in self.layers:
                for neuron in layer.neurons:
                    self._n_weights += neuron.n_inputs + 1  # +1 for bias weight
        return self._n_weights

    def set_weights(self, weights):
        assert len(weights) == self.n_weights, "Incorrect amount of weights."

        stop = 0
        for layer in self.layers:
            for neuron in layer.neurons:
                start, stop = stop, stop + (neuron.n_inputs + 1)
                neuron.set_weights(weights[start:stop])
        return self

    def update(self, inputs):
        assert len(inputs) == self.n_inputs, "Incorrect amount of inputs."

        for layer in self.layers:
            outputs = []
            for neuron in layer.neurons:
                tot = neuron.sum(inputs) + neuron.weights[-1] * BIAS
                outputs.append(self.sigmoid(tot))
            inputs = outputs
        return outputs

    def sigmoid(self, activation, response=1):
        # the activation function
        try:
            return 1 / (1 + math.e**(-activation / response))
        except OverflowError:
            return float("inf")

    def __str__(self):
        return '\n'.join([str(i + 1) + ' ' + str(layer) for i, layer in enumerate(self.layers)])

    def draw_neural_net(self, ax, left, right, bottom, top, layer_sizes):
        '''
        Draw a neural network cartoon using matplotilb.

        :usage:
                >>> fig = plt.figure(figsize=(12, 12))
                >>> draw_neural_net(fig.gca(), .1, .9, .1, .9, [4, 7, 2])

        :parameters:
                - ax : matplotlib.axes.AxesSubplot
                        The axes on which to plot the cartoon (get e.g. by plt.gca())
                - left : float
                        The center of the leftmost node(s) will be placed here
                - right : float
                        The center of the rightmost node(s) will be placed here
                - bottom : float
                        The center of the bottommost node(s) will be placed here
                - top : float
                        The center of the topmost node(s) will be placed here
                - layer_sizes : list of int
                        List of layer sizes, including input and output dimensionality
        '''
        v_spacing = (top - bottom) / float(self.n_neurons_to_hl + 1)
        h_spacing = (right - left) / float(self.n_hidden_layers + 1)
        # Nodes
        for n, layer_size in enumerate(layer_sizes):
            layer_top = v_spacing * (layer_size - 1) / 2. + (top + bottom) / 2.
            for m in range(layer_size):
                circle = plt.Circle((n * h_spacing + left, layer_top - m * v_spacing), v_spacing / 4.,
                                    color='w', ec='k', zorder=4)
                ax.add_artist(circle)
        # Edges
        weights = self.get_weights()
        for n, (layer_size_a, layer_size_b) in enumerate(zip(layer_sizes[:-1], layer_sizes[1:])):
            layer_top_a = v_spacing * (layer_size_a - 1) / 2. + (top + bottom) / 2.
            layer_top_b = v_spacing * (layer_size_b - 1) / 2. + (top + bottom) / 2.
            for m in range(layer_size_a):
                for o in range(layer_size_b):
                    line = plt.Line2D([n * h_spacing + left, (n + 1) * h_spacing + left],
                                      [layer_top_a - m * v_spacing,
                                          layer_top_b - o * v_spacing],
                                      mapToRange(weights[o + m], 0.4, 1.0, 0.1, 3), c='k')
                    ax.add_artist(line)
