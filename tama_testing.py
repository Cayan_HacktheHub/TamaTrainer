import unittest2
from Tamagotchi import Tamagotchi


class TestStringMethods (unittest2.TestCase):

    def test_dirty_to_clean(self):
        test_tama = Tamagotchi()
        test_tama.poop = 5
        test_tama.clean()
        self.assertEqual(test_tama.poop, 0)

    def test_play(self):
        test_tama = Tamagotchi()
        test_tama.energy = 4
        test_tama.happiness = 2
        test_tama.play()
        self.assertEqual(test_tama.happiness, 3)
        self.assertEqual(test_tama.energy, 3)

    def test_sleep_play(self):
        test_tama = Tamagotchi()
        test_tama.energy = 4
        test_tama.sleeping = True
        test_tama.happiness = 1
        test_tama.play()
        self.assertEqual(test_tama.happiness, 0)
        self.assertEqual(test_tama.energy, 4)

    def test_awake_sick_cure(self):
        test_tama = Tamagotchi()
        test_tama.sick = True
        test_tama.happiness = 2
        test_tama.cure()
        self.assertFalse(test_tama.sick)
        self.assertEqual(test_tama.happiness, 0)

    def test_sleeping_sick_cure(self):
        test_tama = Tamagotchi()
        test_tama.sick = True
        test_tama.sleeping = True
        test_tama.happiness = 8
        test_tama.cure()
        self.assertFalse(test_tama.sick)
        self.assertEqual(test_tama.sleeping, False)
        self.assertEqual(test_tama.happiness, 4)

    def test_full_feed_meal(self):
        test_tama = Tamagotchi()
        test_tama.hunger = 0
        test_tama.poop = 0
        test_tama.happiness = 2
        test_tama.feed_meal()
        self.assertEqual(test_tama.hunger, 0)
        self.assertEqual(test_tama.happiness, 1)
        self.assertEqual(test_tama.poop, 1)

    def test_hungry_feed_meal(self):
        test_tama = Tamagotchi()
        test_tama.hunger = 4
        test_tama.happiness = 1
        test_tama.poop = 0
        test_tama.feed_meal()
        self.assertEqual(test_tama.hunger, 3)
        self.assertEqual(test_tama.happiness, 1)
        self.assertEqual(test_tama.poop, 1)

    def test_sleep_sick_feed_meal(self):
        hunger = 4
        happy = 1

        test_tama = Tamagotchi()
        test_tama.sleeping = True
        test_tama.hunger = hunger
        test_tama.happiness = happy
        test_tama.feed_meal()
        self.assertEqual(test_tama.hunger, hunger)
        self.assertEqual(test_tama.happiness, happy)
        self.assertEqual(test_tama.poop, 0)

        del test_tama

        test_tama = Tamagotchi()
        test_tama.sick = True
        test_tama.hunger = hunger
        test_tama.happiness = happy
        test_tama.feed_meal()
        self.assertEqual(test_tama.hunger, hunger)
        self.assertEqual(test_tama.happiness, happy)
        self.assertEqual(test_tama.poop, 0)

    def test_feed_snack(self):
        test_tama = Tamagotchi()
        test_tama.hunger = 4
        test_tama.happiness = 0
        test_tama.feed_snack()
        self.assertEqual(test_tama.hunger, 3)
        self.assertEqual(test_tama.happiness, 1)
        self.assertEqual(test_tama.poop, 1)
        self.assertEqual(test_tama.sickness, 1)

    def test_feed_snack_sleeping(self):
        test_tama = Tamagotchi()
        test_tama.hunger = 4
        test_tama.happiness = 0
        test_tama.sleeping = True
        test_tama.feed_snack()
        self.assertEqual(test_tama.hunger, 4)
        self.assertEqual(test_tama.happiness, 0)
        self.assertEqual(test_tama.poop, 0)
        self.assertEqual(test_tama.sickness, 0)

    def test_dead(self):
        test_tama = Tamagotchi()
        test_tama.hunger = 8
        test_tama._Tamagotchi__check_dead()
        self.assertEqual(test_tama.dead, True)
        del test_tama

        test_tama = Tamagotchi()
        test_tama.age = 182
        test_tama._Tamagotchi__check_dead()
        self.assertEqual(test_tama.dead, True)
        del test_tama

        test_tama = Tamagotchi()
        test_tama.sickness = 8
        test_tama._Tamagotchi__check_dead()
        self.assertEqual(test_tama.dead, True)

    def test_sleeping(self):
        test_tama = Tamagotchi()
        test_tama.energy = 0
        test_tama._Tamagotchi__check_asleep()
        self.assertEqual(test_tama.sleeping, True)
        del test_tama

        test_tama = Tamagotchi()
        test_tama.energy = 4
        test_tama._Tamagotchi__check_asleep()
        self.assertEqual(test_tama.sleeping, False)
        del test_tama

        test_tama = Tamagotchi()
        test_tama.energy = 4
        test_tama.sleeping = True
        test_tama._Tamagotchi__check_asleep()
        self.assertEqual(test_tama.sleeping, False)
        del test_tama

    def test_the_poo(self):
        test_tama = Tamagotchi()
        test_tama.poop = 0
        test_tama.sickness = 0
        test_tama.happiness = 4
        test_tama._Tamagotchi__check_poop()
        self.assertEqual(test_tama.sickness, 0)
        self.assertEqual(test_tama.happiness, 4)
        del test_tama

        test_tama = Tamagotchi()
        test_tama.poop = 3
        test_tama.sickness = 0
        test_tama.happiness = 4
        test_tama._Tamagotchi__check_poop()
        self.assertEqual(test_tama.sickness, 1)
        self.assertEqual(test_tama.happiness, 3)

    def test_sick(self):
        test_tama = Tamagotchi()
        test_tama.sickness = 0
        test_tama._Tamagotchi__check_sick()
        self.assertEqual(test_tama.sick, False)
        del test_tama

        test_tama = Tamagotchi()
        test_tama.sickness = 4
        test_tama._Tamagotchi__check_sick()
        self.assertEqual(test_tama.sick, True)

    def test_update_hunger(self):
        test_tama = Tamagotchi()
        test_tama.hunger = test_tama.limits[0]["min"]
        test_tama._Tamagotchi__update_hunger(-1)
        self.assertEqual(test_tama.hunger, test_tama.limits[0]["min"])

        test_tama.hunger = test_tama.limits[0]["max"]
        test_tama._Tamagotchi__update_hunger(1)
        self.assertEqual(test_tama.hunger, test_tama.limits[0]["max"])

    def test_update_happiness(self):
        test_tama = Tamagotchi()
        test_tama.happiness = test_tama.limits[1]["min"]
        test_tama._Tamagotchi__update_happiness(-1)
        self.assertEqual(test_tama.happiness, test_tama.limits[1]["min"])

        test_tama.happiness = test_tama.limits[1]["max"]
        test_tama._Tamagotchi__update_happiness(1)
        self.assertEqual(test_tama.happiness, test_tama.limits[1]["max"])

    def test_update_sickness(self):
        test_tama = Tamagotchi()
        test_tama.sickness = test_tama.limits[6]["min"]
        test_tama._Tamagotchi__update_sickness(-1)
        self.assertEqual(test_tama.sickness, test_tama.limits[6]["min"])

        test_tama.sickness = test_tama.limits[6]["max"]
        test_tama._Tamagotchi__update_sickness(1)
        self.assertEqual(test_tama.sickness, test_tama.limits[6]["max"])

    def test_update_poop(self):
        test_tama = Tamagotchi()
        test_tama.poop = test_tama.limits[5]["min"]
        test_tama._Tamagotchi__update_poop(-1)
        self.assertEqual(test_tama.poop, test_tama.limits[5]["min"])

        test_tama.poop = test_tama.limits[5]["max"]
        test_tama._Tamagotchi__update_poop(1)
        self.assertEqual(test_tama.poop, test_tama.limits[5]["max"])

    def test_update_energy(self):
        test_tama = Tamagotchi()
        test_tama.energy = test_tama.limits[4]["min"]
        test_tama._Tamagotchi__update_energy(-1)
        self.assertEqual(test_tama.energy, test_tama.limits[4]["min"])

        test_tama.energy = test_tama.limits[4]["max"]
        test_tama._Tamagotchi__update_energy(1)
        self.assertEqual(test_tama.energy, test_tama.limits[4]["max"])

    def test_running_tama(self):
        test_tama = Tamagotchi()
        test_tama.energy = 1
        test_tama.run(-1)
        self.assertEqual(test_tama.hunger, 0)
        self.assertEqual(test_tama.happiness, 8)
        self.assertEqual(test_tama.sickness, 0)
        self.assertEqual(test_tama.energy, 1)
        self.assertEqual(test_tama.tick, 1)
        self.assertEqual(test_tama.age, 0)

        test_tama.tick = 2
        test_tama.energy = 2
        test_tama.happiness = 2
        test_tama.run(-1)
        self.assertEqual(test_tama.hunger, 1)
        self.assertEqual(test_tama.happiness, 1)
        self.assertEqual(test_tama.sickness, 0)
        self.assertEqual(test_tama.energy, 1)
        self.assertEqual(test_tama.tick, 3)

        test_tama.tick = 23
        test_tama.run(-1)
        self.assertEqual(test_tama.age, 1)


if __name__ == '__main__':
    unittest2.main()
