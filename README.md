[![pipeline status](https://gitlab.com/Cayan_HacktheHub/TamaTrainer/badges/master/pipeline.svg)](https://gitlab.com/Cayan_HacktheHub/TamaTrainer/commits/master)  
# TamaTrainer
## A Hack the Hub project by Team Cayan

### General Overview

This program will spawn X tournaments of Y tamagotchis. Each Tam
has its own neural network, initialised with random "dna" (weights)
which will give us a diverse population.

After they have all went to tamagotchi heaven, the tournaments are reset
and the "fittest" tam AIs get reinserted into the next generation, along 
with a set of fresh, random tams. This ensures we don't over-fit the
model, and also provides some momentum to get over local minima.

Ideally, the next generation would also contain cross-bred tamagotchis, 
with "dna" from two parent tamagotchis. This was a bit beyond our 
time at the hackathon, but it's really cool! 

@Andernoo (twitter)  
@Andrew_Cayan (HtH Slack)

### Why?
If you can look after a virtual pet, you can help look after a real one. Maybe even a people.

### How?
Run main.py and let the wonder of nature take control. 

[Natural Selection in Action](https://i.imgur.com/uxh5JfC.png)

Jumps like this are normal, and you can expect a few hundred generations to go past without any 
significant advances. This will improve with the project.


We don't have many dependencies, but if we did they could be installed with:
```
pip install -U -r requirements.txt
```
main.py is the starting point for our AI. Spawn, kill, adapt, survive.

```
python main.py 
```
You can also spawn a single tama that you can play with. Cute!

```
python tama_game.py 
```
