import os
import threading
import signal
import numpy
import random
from TamagotchiAI import TamagotchiAI

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'


class Tournament:
	def __init__(self, tournamentId, numTams):
		self.tournamentId = tournamentId
		self.numTams = numTams
		self.running = True

		self.tams = []

		self.regen()

		print('Tournament ' + self.tournamentId + ' initialised')

	def start(self):
		for tam in self.tams:
			if(tam.threaded):
				tam.run()
			else:
				tam.start()

	def run(self):
		self.running = False
		for tam in self.tams:
			if tam.running:
				self.running = True

	def regen(self, topTams=None):
		for tam in self.tams:
			del tam
		# self.tams.clear()
		if topTams:
			self.tams.append(TamagotchiAI(str(self.tournamentId) + '-1', topTams[0].getWeights()))
			for i in range(1, 14):
				self.tams.append(TamagotchiAI(str(self.tournamentId) + '-' + str(i), random.choice(topTams).getWeights()))
			for i in range(15, 30):
				self.tams.append(TamagotchiAI(str(self.tournamentId) + '-' + str(i)))
			self.tams = self.tams[-30:]
		else:
			for i in range(0, self.numTams):
				self.tams.append(TamagotchiAI(str(self.tournamentId) + '-' + str(i)))
		# print(len(self.tams), self.numTams)
		assert(len(self.tams) == self.numTams)

	def getStats(self):
		stats = {}
		data = []
		for tam in self.tams:
			data.append(tam.fitness)
		stats['max'] = max(data)
		stats['min'] = min(data)
		stats['avg'] = int(numpy.mean(data))
		return stats

	def getTop(self, number):
		if number > len(self.tams):
			print('Number requested ({0}) is greater than number of tams ({1})'.format(
				number, len(self.tams)))
			number = len(self.tams)
		if self.running:
			print('Getting top {0} even though tournament {1} is still running'.format(
				number, self.tournamentId))

		self.tams.sort(key=lambda tam: tam.fitness, reverse=True)
		return self.tams[:number]
