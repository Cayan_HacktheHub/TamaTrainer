import sys
import time
import pygame
import pygame_textinput
from Tamagotchi import Tamagotchi

green = (0, 255, 0)
bright_green = (255, 255, 0)
black = (0, 0, 0)
white = (255, 255, 255)

clock = pygame.time.Clock()


class Button:
    def __init__(self, screen, x, y, w, h, text):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.screen = screen
        self.text = text

    def add_button(self):
        mouse = pygame.mouse.get_pos()
        if self.x + self.w > mouse[0] > self.x and self.y + self.h > mouse[1] > self.y:
            pygame.draw.rect(self.screen, bright_green, (self.x, self.y, self.w, self.h))
        else:
            pygame.draw.rect(self.screen, green, (self.x, self.y, self.w, self.h))
        small_text = pygame.font.SysFont('Calibri', 28)
        text_surf, text_rect = text_objects(self.text, small_text)
        text_rect.center = self.x + (self.w / 2), self.y + (self.h / 2)
        self.screen.blit(text_surf, text_rect)

    def check_over(self):
        mouse = pygame.mouse.get_pos()
        if self.x + self.w > mouse[0] > self.x and self.y + self.h > mouse[1] > self.y:
            return True
        else:
            return False


class Text:
    def __init__(self, screen, x, y, data_name):
        self.x = x
        self.y = y
        self.screen = screen
        self.data_name = data_name
        self.small_text = pygame.font.SysFont('Calibri', 28)
        self.text_surface = 0
        self.text_rect = 0

    def add_text(self, value):
        self.text_surface, self.text_rect = text_objects(self.data_name + ": " + str(value), self.small_text)
        self.text_rect.center = self.x, self.y
        self.screen.blit(self.text_surface, self.text_rect)


def text_objects(text, font):
    textSurface = font.render(text, True, black)
    return textSurface, textSurface.get_rect()


def game_loop():
    pygame.init()
    pygame.display.set_caption('Tamagotchi')

    size = width, height = 800, 600

    screen = pygame.display.set_mode(size)
    screen.fill(white)

    textinput = pygame_textinput.TextInput()

    clean = Button(screen, 125, 200, 170, 100, "0: Clean")
    play = Button(screen, 325, 200, 170, 100, "1: Play")
    cure = Button(screen, 525, 200, 170, 100, "2: Cure")
    meal = Button(screen, 125, 350, 170, 100, "3: Feed Meal")
    snack = Button(screen, 325, 350, 170, 100, "4: Feed Snack")
    nothing = Button(screen, 525, 350, 170, 100, "5: Do Nothing")

    hunger = Text(screen, 200, 50, "Hunger")
    happiness = Text(screen, 200, 75, "Happiness")
    age = Text(screen, 200, 100, "Age")
    sickness = Text(screen, 200, 125, "Sickness")
    sick = Text(screen, 600, 50, "Sick")
    poop = Text(screen, 600, 75, "Poop")
    sleeping = Text(screen, 600, 100, "Sleeping")
    energy = Text(screen, 600, 125, "Energy")
    dead = Text(screen, 200, 150, "Dead")

    flip = 0
    pygame.time.set_timer(pygame.USEREVENT, 500)
    myimage = pygame.image.load("sprites/dance_1.gif")

    game_exit = False

    tama = Tamagotchi()

    while not game_exit:
        screen.fill(white)

        clean.add_button()
        meal.add_button()
        snack.add_button()
        play.add_button()
        cure.add_button()
        nothing.add_button()

        hunger.add_text(tama.hunger)
        happiness.add_text(tama.happiness)
        age.add_text(tama.age)
        sickness.add_text(tama.sickness)
        sick.add_text(tama.sick)
        poop.add_text(tama.poop)
        sleeping.add_text(tama.sleeping)
        energy.add_text(tama.energy)
        dead.add_text(tama.dead)

        # Add the tagline
        small_text = pygame.font.SysFont('Calibri', 28)
        text_surf, text_rect = text_objects("Brought to you by the Pigeon Wranglers", small_text)
        text_rect.center = 230, 580
        screen.blit(text_surf, text_rect)

        pygame.draw.rect(screen, (230, 230, 230), (380, 485, 50, 50))

        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
            elif event.type == pygame.MOUSEBUTTONUP and not tama.dead:
                if clean.check_over():
                    print("Click " + clean.text)
                    tama.run(0)
                elif meal.check_over():
                    print("Click " + meal.text)
                    tama.run(3)
                elif snack.check_over():
                    print("Click " + snack.text)
                    tama.run(4)
                elif play.check_over():
                    print("Click " + play.text)
                    tama.run(1)
                elif cure.check_over():
                    print("Click " + cure.text)
                    tama.run(2)
                elif nothing.check_over():
                    print("Click " + nothing.text)
                    tama.run(-1)
            elif event.type == pygame.USEREVENT and not tama.dead:
                if flip == 0:
                    myimage = pygame.image.load("sprites/dance_1.gif")
                    flip = 1
                else:
                    myimage = pygame.image.load("sprites/dance_2.gif")
                    flip = 0
            elif event.type == pygame.USEREVENT and tama.dead:
                myimage = pygame.image.load("sprites/dead.gif")
            # Feed it with events every frame
            if textinput.update(events):
                inputed_text = textinput.get_text()
                if not inputed_text == '':
                    if 0 <= int(inputed_text) <= 5 and not tama.dead:
                        tama.run(int(inputed_text))
                textinput.clear_text()
        # Blit its surface onto the screen
        textinput.update(events)
        screen.blit(textinput.get_surface(), (400, 500))

        imagerect = myimage.get_rect

        screen.blit(myimage, (300, 0))

        pygame.display.update()
        clock.tick(60)


if __name__ == '__main__':
    game_loop()
